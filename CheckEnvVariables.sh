counter=0

envVariables=(
    "AWS_ACCESS_KEY_ID"
    "AWS_SECRET_ACCESS_KEY"
    "AWS_ACCESS_KEY_ID_CLIENT"
    "AWS_SECRET_ACCESS_KEY_CLIENT"
    "AWS_REGION"
    "AWS_ECR_REGISTRY"
    "AWS_ECR_REGISTRY_STAGING"
    "AWS_ECR_REGISTRY_PROD"
    "EB_ENV_NAME_PREFIX"
    "EB_ENV_NAME_STAGING"
    "EB_ENV_NAME_PROD"
    "S3_BUCKET_NAME_PREFIX"
    "S3_BUCKET_NAME_STAGING"
    "S3_BUCKET_NAME_PROD"
    "TYPEORM_CONNECTION"
    "TYPEORM_DATABASE"
    "TYPEORM_HOST"
    "TYPEORM_USERNAME"
    "TYPEORM_PASSWORD"
    "TYPEORM_PORT"
    "NODE_ENV"
    "AUTH0_CLIENT_ID_DEV"
    "AUTH0_DOMAIN_DEV"
    
)

alertMissingVariable() {
    echo "$1 is unset"; counter=$((counter+1));
}

for var in "${envVariables[@]}"; do
    eval "varValue=\${$var+x}"
    if [ -z $varValue ]; then alertMissingVariable $var; fi
done

exit $counter