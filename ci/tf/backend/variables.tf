variable "access_key" {}
variable "secret_key" {}
variable "aws_region" {}
variable "name_prefix" {}
variable "vpc_cidr" {}
variable "key_name" {}
variable "dev_instance_type" {}
variable "dev_ami" {}
variable "lc_instance_type" {}
variable "environment" {}
variable "application" {}

variable "project" {}
variable "branch" {}
variable "user" {}
variable "password" {}

variable "min_size_alb" {}
variable "max_size_alb" {}
variable "health_check_grace_period" {
  default = "300"
}
variable "health_check_type" {
  default = "ELB"
}

variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}
variable "zone_id" {}
variable "hosted_zone_name" {}
variable "db_snapshot_name" {}
variable "allowed_ssh_ip" {}
variable "db_port" {}