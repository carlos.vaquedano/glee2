variable "dev_instance_type" {}
variable "dev_ami" {}
variable "name_prefix" {}
variable "key_name" {}

variable "public1_subnet_id" {}
variable "public2_subnet_id" {}
variable "private1_subnet_id" {}
variable "private2_subnet_id" {}
variable "bastion_sg_id" {}
variable "sprint_private_sg" {}

variable "lc_instance_type" {}
