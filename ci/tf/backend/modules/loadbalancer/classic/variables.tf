variable "name_prefix" {}
variable "public1_subnet_id" {}
variable "public2_subnet_id" {}
variable "sprint0_public_sg" {}

variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}
variable "zone_id" {}

