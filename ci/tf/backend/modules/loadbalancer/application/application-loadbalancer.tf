data "aws_acm_certificate" "certificate" {
  domain   = "*.acklenavenueclient.com"
  statuses = ["ISSUED"]
}

resource "aws_lb" "application-loadbalancer" {
  name = "${var.name_prefix}-app-lb"
  internal           = false
  load_balancer_type = "application"
  subnets = [var.public1_subnet_id,
    var.public2_subnet_id,
  ]

  security_groups = [var.sprint0_public_sg]

  enable_deletion_protection = false
  tags = {
    Name = "${var.name_prefix}-elb"
  }
}


resource "aws_lb_listener" "app-lb-listener-443" {
  load_balancer_arn = aws_lb.application-loadbalancer.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.certificate.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_lb_tgt_atscaling.arn
  }
}

resource "aws_lb_listener" "app-lb-listener-80" {
  load_balancer_arn = aws_lb.application-loadbalancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_lb_tgt_atscaling.arn
  }
}

resource "aws_lb_target_group" "app_lb_tgt_atscaling" {
  name = "${var.name_prefix}-app-tgt-gp"
  port     = 5000
  protocol = "HTTP"
  vpc_id      = var.vpc_id

  health_check {
    protocol = "HTTP"
    path = "/api/status"
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
  }

  tags = {
    Name = "${var.name_prefix}-app-tgt-gp"
  }

}

resource "aws_route53_record" "backend-dns-record" {
  zone_id = var.zone_id
  name    = "${var.name_prefix}.acklenavenueclient.com"
  type    = "A"

  alias {
    name    = aws_lb.application-loadbalancer.dns_name
    zone_id = aws_lb.application-loadbalancer.zone_id
    evaluate_target_health = "false"
  }
}
