variable "name" {}

variable "name_prefix" {}

variable "aws_certificate_arn" {}

variable "hosted_zone_name" {}

variable "zone_id" {}
