## CI Setup

### AWS Setup

#### Route53

The domain name should be under the control of Route53 since new CNAME entries are created using Terraform.

#### Certificate Manager

Create a "wildcard certificate" using `*.domainname.com` instead of just `domainname.com`. The ARN of that cert should be added to the CI/CD Environment Variables using key `AWS_CERTIFICATE_ARN`.

#### RDS

For db automation to work in CI/CD, you will need a Postgres database already created AND a snapshot. The name of the snapshot should be added to the CI/CD Environment Variables using key `DB_SNAPSHOT_NAME`.

#### Container Registry

Terraform won't create your ECR, you should create it beforehand and add its URI in the `AWS_ECR_REGISTRY` Gitlab CI/CD variables.

#### Key pair

Terraform won't create your EC2s key pair either. You should create it yourself as a pem format. Then, fill `SSH_KEY_NAME` with it's name and `AGENT_SSH_KEY` with the pem file content.