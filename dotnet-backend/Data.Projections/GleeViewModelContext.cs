using System.Collections.Generic;
using Avenue.Persistence.EntityFramework;
using Avenue.Persistence.Relational;
using Microsoft.EntityFrameworkCore;

namespace Data.Projections
{
    public class GleeViewModelContext : CodeFirstDataContext
    {
        public GleeViewModelContext(DbContextOptions options,
            IEnumerable<IModelBuildingStrategy> modelBuildingStrategies) : base(options, modelBuildingStrategies)
        {
        }
    }
}