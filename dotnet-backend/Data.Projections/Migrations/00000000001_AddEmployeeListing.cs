using System;
using Avenue.Persistence.Relational;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Projections.Migrations
{
    [DbContext(typeof(GleeViewModelContext))]
    //EE3: START
    [Migration("00000000001_AddEmployeeListing")]
    public class AddEmployeeListingMigration : Migration
    //EE3: END
    //RP3: START
    //[Migration("00000000001_AddEmployee")]
    //public class AddEmployeeMigration : Migration
        //RP3: END
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            if(migrationBuilder==null)
                throw new ArgumentNullException(nameof(migrationBuilder));
            migrationBuilder.CreateTable(
                //EE4: START
                "EmployeeListing",
                //EE4: END
                //RP4: START
                //"Employee",
                //RP4: END
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: false),
                    SecondLastName = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: false),
                    CompanyEmail = table.Column<string>(nullable: false),
                    PersonalEmail = table.Column<string>(nullable: true),
                    Birthdate = table.Column<DateTime>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: false),
                    Tags = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: false),
                    Region = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Salary = table.Column<double>(nullable: false),
                    EffectiveDate = table.Column<DateTime>(nullable: false),
                    SalaryType = table.Column<string>(nullable: false),
                    Removed = table.Column<bool>(),
                    IsActive = table.Column<bool>()
                },
                //EE:5 START
                constraints: table => { table.PrimaryKey("PK_EmployeeListings", x => x.Id); });
                //EE5: END
                //RP5: START
                //constraints: table => { table.PrimaryKey("PK_Employee", x => x.Id); });
                //RP5: END
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            if(migrationBuilder==null)
                throw new ArgumentNullException(nameof(migrationBuilder));
            //EE6: START
            migrationBuilder.DropTable(
                "EmployeeListings");
            //EE6: END
            //RP6: START
            // migrationBuilder.DropTable(
            //     "Employee");
            //RP6: END
        }
    }
}