using System;
using System.Threading.Tasks;
using Avenue.Domain;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeNamesUpdater))]
    public class when_handling_a_change_employee_display_name_command
    {
        const string NewName = "new name";
        static IWritableRepository<Employee> _writableRepository;
        static EmployeeNamesUpdater _handler;
        static ChangeEmployeeDisplayName _command;
        static Employee _employee;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee>>();

            _employee = Mock.Of<Employee>();

            _command = Builder<ChangeEmployeeDisplayName>.CreateNew()
                .With(x => x.Id, _employee.Id)
                .With(x => x.DisplayName, NewName)
                .Build();

            Mock.Get(_writableRepository).Setup(x => x.Find(_employee.Id))
                .Returns(Task.FromResult(_employee));

            _handler = new EmployeeNamesUpdater(_writableRepository);
        };

        Because of = () => _handler.Handle(_command).Await();

        It should_update_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Update(Moq.It.Is<Employee>(employee => employee.ShouldBeEquivalent(_employee))));
        };

        It should_tell_the_employee_to_change_the_display_name = () =>
        {
            Mock.Get(_employee).Verify(x => x.ChangeDisplayName(NewName));
        };
    }
}