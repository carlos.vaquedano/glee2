using System;
using Avenue.Domain;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    public class when_changing_an_employee_personal_email
    {
        const string NewPersonalEmail = "new personal email";
        static IWritableRepository<Employee> _repo;
        static Guid _employeeId;
        static Employee _employee;
        static EmployeePersonalEmailChanger _systemUnderTest;

        Establish context = () =>
        {
            _employeeId = Guid.NewGuid();
            _employee = Mock.Of<Employee>();

            _repo = Mock.Of<IWritableRepository<Employee>>();

            Mock.Get(_repo).Setup(x => x.Find(Moq.It.Is<Guid>(id => id == _employeeId))).ReturnsAsync(_employee);

            _systemUnderTest = new EmployeePersonalEmailChanger(_repo);
        };

        Because of = () =>
        {
            _systemUnderTest.Handle(new ChangeEmployeePersonalEmail(_employeeId, NewPersonalEmail)).Await();
        };

        It should_change_the_personal_email = () =>
        {
            Mock.Get(_employee).Verify(x => x.ChangePersonalEmail(NewPersonalEmail));
        };

        It should_save_changes_to_database = () => { Mock.Get(_repo).Verify(x => x.Update(_employee)); };
    }
}