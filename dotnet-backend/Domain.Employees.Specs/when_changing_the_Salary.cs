using System;
using System.Linq;
using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_changing_the_Salary
    {
        static Employee _systemUnderTest;
        static double _newSalary;

        Establish _context = () =>
        {
            _newSalary = 1000.2;
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Id, Guid.NewGuid())
                .Build();
        };

        Because of = () => { _systemUnderTest.ChangeSalary(_newSalary); };

        It should_change_the_salary = () => { _systemUnderTest.Salary.Should().Be(_newSalary); };

        It should_notify_the_world_of_the_change = () =>
            _systemUnderTest.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeSalaryChanged(_systemUnderTest.Id, _newSalary));
    }
}