using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Common.Entities;
using Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class ReadOnlyRepository<T, TPKey> : IReadOnlyRepository<T, TPKey> where T : class, IAggregate
    {
        public ReadOnlyRepository(AppDataContext context)
        {
            Context = context;
        }

        AppDataContext Context { get; }

        public async Task<IEnumerable<T>> FindAll()
        {
            return await Context.Set<T>()
                .Where(x => !x.Removed).ToListAsync();
        }

        public Task<T> GetById(TPKey id)
        {
            var entity = Context.Set<T>().FindAsync(id);
            Context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public async Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression,
            bool allowRemoved = false)
        {
            var list = Context.Set<T>()
                .Where(expression);

            if (!allowRemoved)
                list = list.Where(x => !x.Removed);

            return await list.ToListAsync();
        }
    }
}