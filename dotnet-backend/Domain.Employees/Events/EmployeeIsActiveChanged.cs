using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeIsActiveChanged : IDomainEvent
    {
        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public bool IsActive { get; protected set; } = true;

        public EmployeeIsActiveChanged(Guid employeeId, bool isActive)
        {
            EmployeeId = employeeId;
            IsActive = isActive;
        }
    }
}