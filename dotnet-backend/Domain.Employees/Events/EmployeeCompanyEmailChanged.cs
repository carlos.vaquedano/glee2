using System;
using Avenue.Domain;

namespace Domain.Employees.Events
{
    public class EmployeeCompanyEmailChanged : IDomainEvent
    {
        protected EmployeeCompanyEmailChanged()
        {
            
        }

        public Guid EmployeeId { get; protected set; } = Guid.Empty;
        public string CompanyEmail { get; set; } = "";

        public EmployeeCompanyEmailChanged(Guid id, string companyEmail)
        {
            EmployeeId = id;
            CompanyEmail = companyEmail;
        }
    }
}