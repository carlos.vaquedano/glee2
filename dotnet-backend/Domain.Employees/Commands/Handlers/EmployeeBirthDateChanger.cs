using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeBirthDateChanger : ICommandHandler<ChangeEmployeeBirthDate>
    {
        readonly IWritableRepository<Employee> _repo;

        public EmployeeBirthDateChanger(IWritableRepository<Employee> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeBirthDate command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _repo.Find(command.Id);
            employee.ChangeBirthDate(command.BirthDate);
            await _repo.Update(employee);
        }
    }
}