using System;
using System.Threading.Tasks;
using Avenue.Commands;
using Avenue.Domain;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeNamesUpdater : ICommandHandler<ChangeEmployeeNames>, ICommandHandler<ChangeEmployeeDisplayName>
    {
        readonly IWritableRepository<Employee> _writableRepository;

        public EmployeeNamesUpdater(IWritableRepository<Employee> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(ChangeEmployeeNames command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.ChangeNames(command.FirstName, command.MiddleName, command.LastName, command.SecondLastName);
            await _writableRepository.Update(employee);
        }

        public async Task Handle(ChangeEmployeeDisplayName command)
        {
            if(command==null)
                throw new ArgumentNullException(nameof(command),$"{nameof(command)} cannot be null");
            var employee = await _writableRepository.Find(command.Id);
            employee.ChangeDisplayName(command.DisplayName);
            await _writableRepository.Update(employee);
        }
    }
}