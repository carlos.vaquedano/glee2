using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeSalary : ICommand
    {
        public Guid Id { get; }
        public double Salary { get; }

        public ChangeEmployeeSalary(Guid id, double salary)
        {
            Id = id;
            Salary = salary;
        }
    }
}