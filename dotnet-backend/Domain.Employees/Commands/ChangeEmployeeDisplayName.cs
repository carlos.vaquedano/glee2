using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeDisplayName : ICommand
    {
        protected ChangeEmployeeDisplayName()
        {
        }

        public Guid Id { get; protected set; } = Guid.Empty;
        public string DisplayName { get; protected set; } = "";

        public ChangeEmployeeDisplayName(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}