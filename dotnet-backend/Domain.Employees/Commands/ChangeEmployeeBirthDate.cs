using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeBirthDate : ICommand
    {
        public Guid Id { get; }
        public DateTime BirthDate { get; }

        public ChangeEmployeeBirthDate(Guid id, DateTime birthDate)
        {
            Id = id;
            BirthDate = birthDate;
        }
    }
}