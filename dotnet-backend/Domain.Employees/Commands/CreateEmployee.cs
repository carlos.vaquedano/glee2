using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class CreateEmployee : ICommand
    {
        protected CreateEmployee()
        {
        }

        public Guid Id { get; protected set; } = Guid.Empty;
        public string FirstName { get; private set; } = "";
        public string? MiddleName { get; private set; }
        public string LastName { get; private set; } = "";
        public string? SecondLastName { get; private set; }
        public string DisplayName { get; private set; } = "";
        public string CompanyEmail { get; private set; } = "";
        public string? PersonalEmail { get; private set; }
        public DateTime Birthdate { get; private set; } = DateTime.MinValue;
        public DateTime StartDate { get; private set; } = DateTime.MinValue;
        public string Address { get; private set; } = "";
        public string PhoneNumber { get; private set; } = "";
        public string BankName { get; private set; } = "";
        public string AccountNumber { get; private set; } = "";
        public string Gender { get; private set; } = "";
        public string Tags { get; private set; } = "";
        public string Country { get; private set; } = "";
        public string Region { get; private set; } = "";
        public string City { get; private set; } = "";
        public double Salary { get; private set; } = 0;
        public DateTime EffectiveDate { get; private set; } = DateTime.MinValue;
        public string SalaryType { get; private set; } = "";

        public CreateEmployee(Guid id, string firstName, string displayName, string lastName, string gender,
            double salary,
            string? middleName, string? secondLastName, string companyEmail, string personalEmail, DateTime birthdate,
            DateTime startDate, string address, string phoneNumber, string bankName, string accountNumber, string tags,
            string country, string region, string city, DateTime effectiveDate, string salaryType)
        {
            Id = id;
            FirstName = firstName;
            DisplayName = displayName;
            LastName = lastName;
            Gender = gender;
            MiddleName = middleName;
            SecondLastName = secondLastName;
            CompanyEmail = companyEmail;
            PersonalEmail = personalEmail;
            Birthdate = birthdate;
            StartDate = startDate;
            Address = address;
            PhoneNumber = phoneNumber;
            BankName = bankName;
            AccountNumber = accountNumber;
            Tags = tags;
            Country = country;
            Region = region;
            City = city;
            EffectiveDate = effectiveDate;
            SalaryType = salaryType;
            Salary = salary;
        }
    }
}