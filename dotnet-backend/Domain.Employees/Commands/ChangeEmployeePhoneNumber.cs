using System;
using Avenue.Commands;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeePhoneNumber : ICommand
    {
        public Guid Id { get; }
        public string PhoneNumber { get; }

        public ChangeEmployeePhoneNumber(Guid id, string phoneNumber)
        {
            Id = id;
            PhoneNumber = phoneNumber;
        }
    }
}