﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotenv.net;
using Loggly;
using Loggly.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Web.Infrastructure;

namespace Web
{
    [Boilerplate]
    public class Program
    {
        public static int Main(string[] args)
        {
            DotEnv.Config(false);


            var context = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console();

            OptionallyConfigureLogglyLogger(context);

            Log.Logger = context.CreateLogger();

            try
            {
                Log.Information("Starting web host");
                CreateWebHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        static void OptionallyConfigureLogglyLogger(LoggerConfiguration context)
        {
            var applicationName = Environment.GetEnvironmentVariable("LOGGLY_APPLICATION_NAME");
            var customerToken = Environment.GetEnvironmentVariable("LOGGLY_CUSTOMER_TOKEN");
            var endpointHost = Environment.GetEnvironmentVariable("LOGGLY_ENDPOINT_HOSTNAME");
            var endpointPort = Environment.GetEnvironmentVariable("LOGGLY_ENDPOINT_PORT");

            var emptyConfigVars =
                new List<string> {applicationName, customerToken, endpointHost, endpointPort}.Select(
                    string.IsNullOrEmpty);
            if (emptyConfigVars.Any())
                return;

            var config = LogglyConfig.Instance;
            config.CustomerToken = customerToken;
            config.ApplicationName = applicationName;
            config.Transport.EndpointHostname = endpointHost;
            config.Transport.EndpointPort = Convert.ToInt32(endpointPort);
            config.Transport.LogTransport = LogTransport.Https;

            var ct = new ApplicationNameTag {Formatter = "application-{0}"};
            config.TagConfig.Tags.Add(ct);

            context.WriteTo.Loggly();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(app => app
                    .AddEnvironmentVariables())
                .UseStartup<Startup>()
                .UseSerilog();
        }
    }
}