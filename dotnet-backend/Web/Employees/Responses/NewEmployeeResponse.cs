using System;

namespace Web.Employees.Responses
{
    public class NewEmployeeResponse
    {
        public Guid Id { get; }

        public NewEmployeeResponse(Guid id)
        {
            Id = id;
        }
    }
}