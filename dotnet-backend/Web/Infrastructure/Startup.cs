﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Web.Infrastructure.Bootstrapping;
using Web.Infrastructure.Bootstrapping.Dependencies;
using Web.Infrastructure.ExceptionHandling;

namespace Web.Infrastructure
{
    [Boilerplate]
    public class Startup
    {
        IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            IdentityModelEventSource.ShowPII = true;

            services
                .ConfigureEnvironmentVariables()
                .ConfigureAuthentication(
                    Configuration["JWT_ISSUER"],
                    Configuration["JWT_AUDIENCE"])
                .ConfigureWebDependencies()
                .ConfigureAutoMapper()
                .ConfigureAuxiliaryServices()
                .ConfigureCommandEventSystem(Configuration);
            
            if (Configuration.IsDevelopment()) 
                services.ConfigureSwagger();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (Configuration.IsDevelopment())
                app
                    .SetupAppForDevelopmentMode()
                    .UseDeveloperExceptionPage();
            else
                app.UseHsts();
            
            
            app.UseAuthentication();
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseMvc();
        }
    }
}