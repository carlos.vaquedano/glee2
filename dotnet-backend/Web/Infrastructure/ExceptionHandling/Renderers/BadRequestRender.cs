using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Web.Infrastructure.ExceptionHandling.Exceptions;

namespace Web.Infrastructure.ExceptionHandling.Renderers
{
    [Boilerplate]
    public class BadRequestRender : IExceptionRender
    {
        public bool ShouldHandle(Exception exception)
        {
            return exception is BadRequestException;
        }

        public Task Render(HttpContext context, Exception exception)
        {
            if(context == null)
                throw new ArgumentNullException(nameof(context));
            if(exception == null)
                throw new ArgumentNullException(nameof(exception));
            
            var result = JsonConvert.SerializeObject(new {innerException = exception.InnerException});
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(result);
        }
    }
}