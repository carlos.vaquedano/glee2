using Data;
using Domain.Employees;
using Web.TestingSupport.Support;

namespace Web.TestingSupport
{
    public class EmployeesController : CrudController<Employee>
    {
        public EmployeesController(AppDataContext context) : base(context)
        {
        }
    }
}