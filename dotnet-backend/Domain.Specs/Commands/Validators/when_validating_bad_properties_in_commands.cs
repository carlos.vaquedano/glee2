using Common.Commands.Validators;
using Domain.Commands;
using Domain.Commands.Validators.ForCreateEmployee;
using FizzWare.NBuilder;
using Machine.Specifications;

namespace Domain.Specs.Commands.Validators
{
    public class when_validating_bad_properties_in_commands
    {
        private static CheckProperties _propertiesValidator;
        private static IValidationResult _result;

        private Establish context = () =>
        {
            _propertiesValidator = new CheckProperties();
            _command = Builder<CreateEmployee>.CreateNew().With(employee => employee.FirstName,null).Build();
        };


        private Because of = () =>
            _result = _propertiesValidator.Validate(_command).Await().AsTask.Result;

        private It should_return_errors_for_first_name = () =>
        {
            _result.Errors.ShouldContain(e =>e.Field == "FirstName");
        };

        private static CreateEmployee _command;
    }
}