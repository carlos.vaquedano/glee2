using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Commands.Validators;
using Common.Exceptions;
using Domain.Commands;
using Domain.Commands.Validators.ForCreateEmployee;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Validators
{
    public class when_validating_a_bad_create_employee
    {
        private static CreateEmployeeValidator _createEmployeeValidator;
        private static Exception _result;
        private static CreateEmployee _createEmployee;


        private static IValidationComponent<CreateEmployee> validationComponent;

        private Establish _context = () =>
        {
            validationComponent = Mock.Of<IValidationComponent<CreateEmployee>>();
            
            _createEmployeeValidator = new CreateEmployeeValidator(new [] { validationComponent});
            
            _validationErrors = Builder<ValidationError>.CreateListOfSize(1).Build();
            _createEmployee = Builder<CreateEmployee>.CreateNew().Build();

            Mock.Get(validationComponent)
                .Setup(component => component.Validate(_createEmployee))
                .Returns(Task<IValidationResult>
                    .Factory
                    .StartNew(() => new CommandValidationResult(_validationErrors)));
        };

        private Because of = () =>
            _result = Catch.Exception(() => _createEmployeeValidator.Validate(_createEmployee).Await());

        private It should_return_validation_errors = () =>
        {
            _result.ShouldBeOfExactType<NotValidException>();
            var notValidException = (NotValidException) _result;
            notValidException.Errors.Should().NotBeEmpty();
        };
        private static IList<ValidationError> _validationErrors;
    }
}