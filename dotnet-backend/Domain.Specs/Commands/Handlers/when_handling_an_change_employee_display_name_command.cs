using System;
using System.Threading.Tasks;
using Domain.Commands;
using Domain.Commands.Handlers;
using Domain.Entities;
using Domain.Repositories;
using Domain.Specs.Commands.Helpers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeDisplayNameChanger))]
    public class when_handling_a_change_employee_display_name_command
    {
        const string NewName = "new name";
        static IWritableRepository<Employee, Guid> _writableRepository;
        static EmployeeDisplayNameChanger _handler;
        static ChangeEmployeeDisplayName _command;
        static Employee _employee;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee, Guid>>();

            _employee = Mock.Of<Employee>();

            _command = Builder<ChangeEmployeeDisplayName>.CreateNew()
                .With(x => x.Id, _employee.Id)
                .With(x=> x.DisplayName, NewName)
                .Build();

            Mock.Get(_writableRepository).Setup(x => x.GetById(_employee.Id))
                .Returns(Task.FromResult(_employee));

            _handler = new EmployeeDisplayNameChanger(_writableRepository);
        };

        Because of = () => _handler.Handle(_command).Await();

        It should_update_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Update(Moq.It.Is<Employee>(employee => employee.ShouldBeEquivalent(_employee))));
        };

        It should_tell_the_employee_to_change_the_display_name = () => { Mock.Get(_employee).Verify(x => x.ChangeDisplayName(NewName)); };
    }
}