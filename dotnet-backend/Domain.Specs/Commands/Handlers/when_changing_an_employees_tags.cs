using System;
using Domain.Commands;
using Domain.Commands.Handlers;
using Domain.Entities;
using Domain.Repositories;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Handlers
{
    public class when_changing_an_employees_tags
    {
        static EmployeeTagChanger _systemUnderTest;
        static IWritableRepository<Employee, Guid> _writableRepository;
        static Guid _employeeId;
        static string _newTags;
        static Employee _employee;

        Establish _context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee, Guid>>();
            _systemUnderTest = new EmployeeTagChanger(_writableRepository);

            _employeeId = Guid.NewGuid();
            _newTags = "new tags";

            _employee = Mock.Of<Employee>();
            Mock.Get(_writableRepository).Setup(x => x.GetById(Moq.It.Is<Guid>(i => i == _employeeId)))
                .ReturnsAsync(_employee);
        };

        Because of = () => { _systemUnderTest.Handle(new ChangeEmployeeTags(_employeeId, _newTags)).Await(); };

        It should_change_the_tags_on_the_employee = () =>
        {
            Mock.Get(_employee).Verify(x => x.ChangeTags(Moq.It.Is<Guid>(id => id == _employeeId), _newTags));
        };

        It should_save_the_employee_changes = () => { Mock.Get(_writableRepository).Verify(x => x.Update(_employee)); };
    }

    // ChangeEmployeePersonalEmail
    // ChangeEmployeeCompanyEmail
}