using System;
using Domain.Commands;
using Domain.Commands.Handlers;
using Domain.Entities;
using Domain.Repositories;
using Domain.Services;
using Domain.Specs.Commands.Helpers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Domain.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeCreator))]
    public class when_handling_a_create_employee_command
    {
        static IWritableRepository<Employee, Guid> _writableRepository;

        static EmployeeCreator _handler;
        static CreateEmployee _createEmployee;

        static Employee _expectedEmployeeToSave;
        static IIdentityGenerator<Guid> _identityGenerator;

        private Establish context = () =>
        {
            _identityGenerator = Mock.Of<IIdentityGenerator<Guid>>();
            _writableRepository = Mock.Of<IWritableRepository<Employee, Guid>>();
            _handler = new EmployeeCreator(_writableRepository, _identityGenerator);
            _createEmployee = Builder<CreateEmployee>.CreateNew().Build();

            var employeeId = Guid.NewGuid();
            Mock.Get(_identityGenerator)
                .Setup(x => x.Generate())
                .Returns(employeeId);

            _expectedEmployeeToSave = Builder<Employee>.CreateNew().With(x => x.Id, employeeId)
                .Build();
        };

        Because of = () => _handler.Handle(_createEmployee).Await();


        It should_create_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Create(Moq.It.Is<Employee>(submittedEmployee =>
                    _expectedEmployeeToSave.ShouldBeEquivalent(submittedEmployee))));
        };
    }
}