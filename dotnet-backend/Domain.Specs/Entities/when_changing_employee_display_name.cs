using System;
using System.Linq;
using Domain.Entities;
using Domain.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Domain.Specs.Entities
{
    public class when_changing_employee_display_name
    {
        static Employee _employee;
        static string _newDisplayName;

        Establish context = () =>
        {
            _newDisplayName = "New Display name";
            _employee = Builder<Employee>.CreateNew()
                .With(x=> x.Id, Guid.NewGuid())
                .With(x => x.DisplayName, _newDisplayName).Build();
        };

        Because of = () => { _employee.ChangeDisplayName(_newDisplayName); };

        It should_change_the_display_name = () => { _employee.DisplayName.Should().Be(_newDisplayName); };


        It should_publish_update_event = () =>
        {
            _employee.GetChanges().FirstOrDefault().Should()
                .BeEquivalentTo(new EmployeeDisplaynameChanged(_employee.Id, _newDisplayName));
        };
    }
}