using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Entities;
using Domain.Repositories;

namespace Domain.Commands.Handlers
{
    public class EmployeeRemover : ICommandHandler<RemoveEmployee>
    {
        readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeRemover(IWritableRepository<Employee, Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(RemoveEmployee command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.Remove();
            await _writableRepository.Update(employee);
        }
    }
}