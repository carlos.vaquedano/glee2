using System;

namespace Domain.Commands
{
    public class ChangeEmployeeNames
    {
        protected ChangeEmployeeNames()
        {
        }

        public ChangeEmployeeNames(Guid id, string firstName,
            string middleName, string lastName, string secondLastName)
        {
            Id = id;
            FirstName = firstName;
            MiddleName = middleName;
            SecondLastName = secondLastName;
            LastName = lastName;
        }

        public Guid Id { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public string MiddleName { get; protected set; }
        public string SecondLastName { get; protected set; }
    }
}