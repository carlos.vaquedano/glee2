using System;

namespace Domain.Commands
{
    public class ChangeEmployeePhoneNumber
    {
        public Guid Id { get; }
        public string PhoneNumber { get; }

        public ChangeEmployeePhoneNumber(Guid id, string phoneNumber)
        {
            Id = id;
            PhoneNumber = phoneNumber;
        }
    }
}