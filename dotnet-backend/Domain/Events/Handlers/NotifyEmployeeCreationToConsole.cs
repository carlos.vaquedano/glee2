using System;
using System.Threading.Tasks;
using AcklenAvenue.Events;

namespace Domain.Events.Handlers
{
    public class NotifyEmployeeCreationToConsole : IEventHandler<EmployeeCreated>
    {
        public Task Handle(EmployeeCreated command)
        {
            Console.WriteLine($"Employee {command.Name} was created");
            return Task.CompletedTask;
        }
    }
}