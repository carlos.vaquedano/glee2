using System;
using System.Threading.Tasks;
using AcklenAvenue.Events;

namespace Domain.Events.Handlers
{
    public class NotifyEmployeeRemovedToConsole : IEventHandler<EmployeeRemoved>
    {
        public Task Handle(EmployeeRemoved command)
        {
            return Task.CompletedTask;
        }
    }
}