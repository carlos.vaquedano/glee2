using System;
using Common.Entities;

namespace Domain.Events
{
    public class EmployeeDisplaynameChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string Displayname { get; }


        public EmployeeDisplaynameChanged(Guid employeeId, string displayname)
        {
            EmployeeId = employeeId;
            Displayname = displayname;
        }
    }
}