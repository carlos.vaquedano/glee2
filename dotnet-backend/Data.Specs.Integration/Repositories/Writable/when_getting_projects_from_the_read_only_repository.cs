using System;
using System.Collections.Generic;
using System.Linq;
using AcklenAvenue.Events;
using Data.Repositories;
using Domain.Employees;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Data.Specs.Integration.Repositories.Writable
{
    public class when_getting_projects_from_the_read_only_repository
    {
        static ReadOnlyRepository<Employee, Guid> _systemUnderTest;
        static IEnumerable<Employee> _result;
        static IEnumerable<Employee> _nonRemovedEmployees;

        static IEventDispatcher _eventDispatcher;
        static AppDataContext _appDataContext;

        Cleanup after = () => { _appDataContext.Clean(); };

        Establish context = () =>
        {
            _appDataContext = InMemoryDataContext
                .GetInMemoryContext()
                .Seed();

            _nonRemovedEmployees = _appDataContext.Set<Employee>().Where(x => !x.Removed);
            _eventDispatcher = Mock.Of<IEventDispatcher>();
            _systemUnderTest = new ReadOnlyRepository<Employee, Guid>(_appDataContext);
        };

        Because of = () => { _result = _systemUnderTest.Set(); };

        It should_return_a_list_of_non_removed_projects = () =>
            _result.Where(x=> !x.Removed).Should().BeEquivalentTo(_nonRemovedEmployees);
    }
}