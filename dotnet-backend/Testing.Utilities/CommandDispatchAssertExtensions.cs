using System;
using System.Linq.Expressions;
using Avenue.Commands;
using Avenue.Dispatch;
using Avenue.Testing.Moq;
using Moq;

namespace Testing.Utilities
{
    public static class CommandDispatchAssertExtensions
    {
        public static void ShouldDispatchACommandLike<T>(this Mock<IDispatcher<ICommand>> mock, T comparisonObject)
            where T : ICommand
        {
            if (object.Equals(comparisonObject, default(T))) 
                throw new ArgumentNullException(nameof(comparisonObject));
            
            if(mock == null)
                throw new ArgumentNullException(nameof(mock));

            mock.Verify(x => x.Dispatch(WithEventHandlersFromSome<T>.Like(comparisonObject)));
        }

        public static void ShouldDispatchACommandLike<T>(this Mock<IDispatcher<ICommand>> mock,
            Expression<Func<T, bool>> func) where T : ICommand
        {
            if(mock == null)
                throw new ArgumentNullException(nameof(mock));
            mock.Verify(x => x.Dispatch(WithEventHandlersFromSome<T>.With(func)));
        }
    }
}