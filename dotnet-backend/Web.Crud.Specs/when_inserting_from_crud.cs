using System;
using System.Collections.Generic;
using Data;
using Domain.Employees;
using FizzWare.NBuilder;
using Machine.Specifications;
using Microsoft.AspNetCore.Http;
using Web.TestingSupport;

namespace Web.Crud.Specs
{
    public class when_inserting_from_crud
    {
        static AppDataContext _appDataContext;
        static EmployeesController _crudController;
        static Dictionary<string, string> _properties;
        static Guid _id;
        const string FirstName = "testing1";

        Establish _context = () =>
        {
            var firstDbContext = TestAppDataContext.GetInstance();
            firstDbContext.Set<Employee>().AddAsync(Builder<Employee>.CreateNew()
                .With(x => x.Id, _id)
                .Build());
            firstDbContext.SaveChangesAsync();

            
            _appDataContext = TestAppDataContext.GetInstance();
            _crudController = new EmployeesController(_appDataContext);

            _id = Guid.NewGuid();

            _properties = new Dictionary<string, string>
            {
                {"id", _id.ToString()},
                {"firstName", FirstName},
                {"middleName", "testing2"},
                {"lastName", "testing3"},
                {"secondLastName", "testing4"},
                {"birthDate", "1-1-1980"},
                {"startDate", "1-1-1980"},
                {"removed", "false"}
            };
        };

        Because of = () => { _crudController.Add(_properties).Await(); };

        It should_be_accessible_from_entity_framework = () =>
        {
            var emp = _appDataContext.Set<Employee>().Find(_id);
            emp.FirstName.ShouldEqual(FirstName);
        };

        Cleanup after = () => _appDataContext.Clean();
    }
}