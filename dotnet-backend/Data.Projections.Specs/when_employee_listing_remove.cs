using Domain.Employees.Events;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Projections.Specs
{
    public class when_employee_listing_remove : given_an_employee_listing_event_handler_context
    {
        Establish _context = () =>
        {
            _employeeCreated = Builder<EmployeeCreated>.CreateNew().Build();
            _expectedEmployeeListing = new EmployeeListing(_employeeCreated.EmployeeId, _employeeCreated.FirstName,
                _employeeCreated.MiddleName, _employeeCreated.LastName, _employeeCreated.SecondLastName,
                _employeeCreated.DisplayName, _employeeCreated.CompanyEmail, _employeeCreated.PersonalEmail,
                _employeeCreated.Birthdate, _employeeCreated.StartDate, _employeeCreated.Address,
                _employeeCreated.PhoneNumber, _employeeCreated.BankName, _employeeCreated.AccountNumber,
                _employeeCreated.Gender, _employeeCreated.Tags, _employeeCreated.Country, _employeeCreated.Region,
                _employeeCreated.City, _employeeCreated.Salary, _employeeCreated.SalaryType,
                _employeeCreated.EffectiveDate, true);
        };

        Because of = () => { _expectedEmployeeListing.Remove(); };

        It should_set_the_property_removed_to_true = () => { _expectedEmployeeListing.Removed.Should().Be(true); };
        static EmployeeCreated _employeeCreated;
        static EmployeeListing _expectedEmployeeListing;
    }
}