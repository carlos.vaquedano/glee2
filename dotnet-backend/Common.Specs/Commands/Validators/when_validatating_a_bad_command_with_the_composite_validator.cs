using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Commands.Validators;
using Common.Exceptions;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using It = Machine.Specifications.It;

namespace Common.Specs.Commands.Validators
{
    public class when_validatating_a_bad_command_with_the_composite_validator
    {
        private static CompositeCommandValidator<TestCommand> _compositeValidator;
        private static IValidationComponent<TestCommand> _fakeValidationComponent;
        private static Exception _result;


        private static List<ValidationError> _validationErrors;

        private static IValidationResult _validationResult;

        private Establish _context = () =>
        {
            _fakeValidationComponent = Mock.Of<IValidationComponent<TestCommand>>();
            _compositeValidator = new CompositeCommandValidator<TestCommand>(new[] {_fakeValidationComponent});
            _validationResult = Mock.Of<IValidationResult>();

            _validationErrors = new List<ValidationError>
            {
                new ValidationError
                {
                    Field = "TestField",
                    FieldLabel = "Field Label",
                    Message = "Test Message"
                }
            };

            Mock.Get(_fakeValidationComponent)
                .Setup(component => component.Validate(Moq.It.IsAny<TestCommand>()))
                .Returns(Task<IValidationResult>.Factory.StartNew(() => _validationResult));

            Mock.Get(_validationResult).SetupGet(result => result.HasErrors).Returns(true);

            Mock.Get(_validationResult).SetupGet(result => result.Errors).Returns(_validationErrors);
        };

        private Because of = () =>
            _result = Catch.Exception(() => _compositeValidator.Validate(new TestCommand()).Await());

        private It should_throw_exception_with_correct_message = () =>
        {
            _result.Should().BeOfType<NotValidException>();
            var notValidException = (NotValidException) _result;
            notValidException.Errors.Should().BeEquivalentTo(_validationErrors);
        };
    }

    public class TestCommand
    {
    }
}