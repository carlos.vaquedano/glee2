import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { pickAll } from 'ramda'
import {
  GET_EMPLOYEES_SUCCESS,
  GET_EMPLOYEE_SUCCESS,
  EDIT_EMPLOYEE_SUCCESS,
  GET_EMPLOYEES
} from '@/store/modules/employee/mutation-types'
import { Employee, CreateEmployee, EditEmployeePayload } from '@/types/employee'
import { havePropertiesChanged } from '@/utils'
import { httpService } from '@/services/HttpService/instance'

interface EmployeeState {
  employees: Employee[]
  employee?: Employee
}

@Module({
  name: 'employee',
  namespaced: true,
  stateFactory: true
})
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
export default class EmployeeModule extends VuexModule
  implements EmployeeState {
  public employees = [] as Employee[]
  public employee?: Employee;

  @Mutation
  [GET_EMPLOYEES]() {
    this.employees = [] as Employee[]
    this.employee = undefined
  }

  @Mutation
  [GET_EMPLOYEES_SUCCESS](data: Employee[]) {
    this.employees = data
    this.employee = undefined
  }

  get activeEmployee() {
    return this.employee
  }

  @Mutation
  [GET_EMPLOYEE_SUCCESS](data: Employee) {
    this.employee = data
  }

  @Mutation
  [EDIT_EMPLOYEE_SUCCESS]() {
    this.employee = undefined
  }

  @Action
  getEmployees(): void {
    this.context.commit(GET_EMPLOYEES)
    httpService.get<Employee[]>('employees').then((res: Employee[]) => {
      this.context.commit(GET_EMPLOYEES_SUCCESS, res)
    })
  }

  @Action
  getEmployee(id: string): Promise<Employee> {
    return httpService
      .get<Employee>(`employees/${id}`)
      .then((res: Employee) => {
        this.context.commit(GET_EMPLOYEE_SUCCESS, res)
        return res
      })
  }

  @Action
  createEmployee(data: CreateEmployee): void {
    console.log('dataaaa', data)
    httpService.post('employees', data).then(() => {
      // @ts-ignore
      // eslint-disable-next-line no-undef
      $nuxt.$router.push('/employees')
    })
  }

  @Action({ rawError: true })
  editEmployee(payload: EditEmployeePayload): void {
    this.context
      .dispatch('editEmployeeField', {
        payload,
        fields: ['firstName', 'middleName', 'lastName', 'secondLastName'],
        endpoint: 'names'
      })
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['displayName'],
          endpoint: 'displayName'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['personalEmail'],
          endpoint: 'personalEmail'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['companyEmail'],
          endpoint: 'companyEmail'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['salary'],
          endpoint: 'salary'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['salaryType'],
          endpoint: 'salaryType'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['effectiveDate'],
          endpoint: 'effectiveDate'
        })
      )
      .then(() =>
        this.context.dispatch('editEmployeeField', {
          payload,
          fields: ['birthdate'],
          endpoint: 'birthdate'
        })
      )
      .then(() => {
        this.context.commit(EDIT_EMPLOYEE_SUCCESS)
        // @ts-ignore
        // eslint-disable-next-line no-undef
        $nuxt.$router.push('/employee')
      })
      .catch(() => {})
    // WIP, address and phoneNumber
    // .then(() =>
    //   this.context.dispatch("editEmployeeField", {
    //     payload,
    //     fields: ["phoneNumber"],
    //     endpoint: "phoneNumber"
    //   })
    // )
    // .then(() =>
    //   this.context.dispatch("editEmployeeField", {
    //     payload,
    //     fields: ["city", 'region', 'country'],
    //     endpoint: "address"
    //   })
    // )
  }

  @Action({ rawError: true })
  editEmployeeField({
    payload,
    fields,
    endpoint
  }: {
    payload: EditEmployeePayload
    fields: string[]
    endpoint: string
  }) {
    if (havePropertiesChanged(fields, this.activeEmployee, payload.data)) {
      return httpService.put(
        `employees/${payload.id}/${endpoint}`,
        pickAll(fields, payload.data)
      )
    }
  }

  @Action
  deactivateEmployee(id: string): void {
    httpService.put(`employees/${id}/inactive`, {}).then(() => {
      this.context.dispatch('getEmployees')
    })
  }

  @Action
  activateEmployee(id: string): void {
    httpService.put(`employees/${id}/active`, {}).then(() => {
      this.context.dispatch('getEmployees')
    })
  }

  @Action
  addTags(id: string, tags: string[]): void {
    httpService.put(`employees/${id}/tags`, tags).then(() => {
      this.context.dispatch('getEmployees')
    })
  }
}
