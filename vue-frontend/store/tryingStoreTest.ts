import { Mutation, Action, VuexModule, Module } from 'vuex-module-decorators'

interface TestState {
  wheels: number
}

@Module({
  name: 'testing',
  namespaced: true,
  stateFactory: true
})
export default class Test extends VuexModule implements TestState {
  public wheels = 2

  @Mutation
  incrWheels(extra: number) {
    this.wheels += extra
  }

  @Action({ rawError: true })
  simpleRequest() {
    // eslint-disable-next-line
    console.log('hi request')
  }

  get axles() {
    return this.wheels / 2
  }
}
