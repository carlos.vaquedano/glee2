import { Middleware } from '@nuxt/types'

const isAuthenticated: Middleware = async ({ route, store, $auth0 }) => {
  if (!store.state.auth0.isAuthenticated && !store.state.auth0.user) {
    try {
      await $auth0.getTokenSilently({ timeoutInSeconds: 5 })
    } catch (error) {
      $auth0.loginWithRedirect({ appState: { targetUrl: '/employees' } })
    }
  }
  if (route.fullPath === '/') store.$router.replace('/employees')
}

/*
const logout: Middleware = async ({ $auth0 }) => {
  try {
    await $auth0.logout()
  } catch (error) {
    $auth0.loginWithRedirect({ appState: { targetUrl: '/employees' } })
  }
}
 */
export default isAuthenticated
