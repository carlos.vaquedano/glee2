import { withKnobs } from '@storybook/addon-knobs'
import Sidebar from './Sidebar.vue'

export default { title: 'Sidebar', decorator: [withKnobs], parameters: {} }

export const asAComponent = () => ({
  components: { Sidebar },
  template: `
    <Sidebar />
  `
})
