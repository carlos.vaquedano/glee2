import { withKnobs } from '@storybook/addon-knobs'
import Breadcrumb from './Breadcrumb.vue'

export default {
  title: 'Breadcrumb',
  decorator: [withKnobs],
  parameters: {}
}

export const asAComponent = () => ({
  components: { Breadcrumb },
  data() {
    return {
      menu: [
        {
          text: 'Employees',
          style: this.thirdLevelBreadcrumb
        },
        {
          text: 'View & Manage',
          style: this.thirdLevelBreadcrumb
        }
      ],
      rootPathName: 'Add New Employee'
    }
  },
  template: `
    <Breadcrumb :menu="menu" :rootPathName="rootPathName" />
  `
})
export const asASecondaryPAge = () => ({
  components: { Breadcrumb },
  data() {
    return {
      menu: [
        {
          text: 'Employees',
          style: this.thirdLevelBreadcrumb
        },
        {
          text: 'View & Manage',
          style: this.thirdLevelBreadcrumb
        }
      ],
      isSecondaryPage: true,
      rootPathName: 'Add New Employee'
    }
  },
  template: `
    <Breadcrumb :menu="menu" :isSecondaryPage="isSecondaryPage" :rootPathName="rootPathName" />
  `
})
