import { HttpService } from '@/services/HttpService'
// eslint-disable-next-line
let httpService: HttpService

export function initializeHttpService(service: HttpService) {
  httpService = service
  return httpService
}

export { httpService }
