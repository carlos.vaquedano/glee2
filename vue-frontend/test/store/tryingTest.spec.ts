import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'
import { getModule } from 'vuex-module-decorators'
import TestingModule from '@/store/tryingStoreTest'
import { HttpService } from '@/services/HttpService'

const mockHttpService = {
  get: jest.fn(),
  post: jest.fn()
}
jest.mock('@/services/HttpService/instance', () => {
  return {
    // @ts-ignore
    httpService: jest.fn<HttpService, []>().mockImplementation(() => {
      return mockHttpService
    })
  }
})

const Vue = createLocalVue()
Vue.use(Vuex)

const factory = () => {
  const store = new Vuex.Store({
    modules: {
      testing: TestingModule
    }
  })
  return getModule(TestingModule, store)
}
describe('TestingModule', () => {
  describe('mutations', () => {
    it('should get a store instance', () => {
      const service = factory()
      expect(service).toBeInstanceOf(Object)
    })
    it('should set stories', () => {
      const service = factory()
      service.incrWheels(2)
      expect(service.wheels).toEqual(4)
    })
  })
  describe('getter', () => {
    it('should get axles', () => {
      const service = factory()
      expect(service.axles).toEqual(1)
    })
  })
})
