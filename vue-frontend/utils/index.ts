import moment from 'moment'
import { equals, pickAll } from 'ramda'

export const formatDate = (
  date?:
    | string
    | number
    | void
    | moment.Moment
    | Date
    | (string | number)[]
    | moment.MomentInputObject
    | undefined,
  dateFormat?:
    | string
    | moment.MomentBuiltinFormat
    | (string | moment.MomentBuiltinFormat)[]
    | undefined,
  format: string | undefined = 'MM/DD/YYYY'
): string => moment(date, dateFormat).format(format)

export const havePropertiesChanged = (
  props: string[],
  original: unknown,
  changedObject: unknown
): boolean => !equals(pickAll(props, original), pickAll(props, changedObject))
