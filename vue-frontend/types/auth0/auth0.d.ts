import { NuxtAxiosInstance } from '@nuxtjs/axios'
import Vue from 'vue'
import { Auth0Instance } from '~/plugins/Auth0'

export interface AuthData {
  loggedIn: boolean
  user: any
  token: string
}

declare module 'vue/types/vue' {
  interface Vue {
    $auth0: Auth0Instance
    $authData: AuthData
  }
}

declare module '@nuxt/types/app' {
  interface Context {
    $auth0: Auth0Instance
    $authData: AuthData
    $axios: NuxtAxiosInstance
  }
}
