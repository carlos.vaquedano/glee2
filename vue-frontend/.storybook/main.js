module.exports = {
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-actions/register',
    '@storybook/addon-links',
    '@storybook/addon-knobs',
    '@storybook/addon-links/register'
  ]
}
