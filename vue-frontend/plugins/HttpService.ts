import { Plugin } from '@nuxt/types'
import { initializeHttpService } from '@/services/HttpService/instance'
import { HttpService } from '@/services/HttpService'

const httpServicePlugin: Plugin = ({ $axios, $auth0 }) => {
  const httpServiceInstance = new HttpService($axios, $auth0)
  initializeHttpService(httpServiceInstance)
}

export default httpServicePlugin
