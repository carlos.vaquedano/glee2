import Vue from 'vue'
import moment from 'moment'
declare module 'vue/types/vue' {
  interface Vue {
    $date: typeof moment
  }
}

Vue.prototype.$date = moment
