const axios = require("axios");
const https = require("https");

class Crud {
    constructor(baseURL, headers) {
        this.http = axios.create({
            baseURL,
            timeout: 60000,
            headers,
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });

        this._found = false;
    }

    async find(id) {
        if (this._found) {
            return this._found;
        }

        var result = await this.request("get", id);
        if (!result) {
            throw new Error("Item not found.");
        }

        this._found = result;
        return this._found;
    }

    async create(employee) {
        var result = await this.request("post", '', employee);
        return result;
    }

    async modify(id, changes) {
        var result = await this.request("put", id, changes);
        return result;
    }

    async clearAll() {
        return await this.request("delete");
    }

    async request(verb, route, payload) {
        try {
            var result = await this.http[verb](route, payload);
            return result.data;
        }
        catch (error) {
            var response = error.response || {};
            throw {
                message: `${verb.toUpperCase()} request resulted in an error. ${response.statusText}. ${response.data.error}`,
                status: error.response.status,
                method: error.config.method,
                url: error.config.url,
                payload: error.config.data
            };
        }
    }
}

module.exports = {
    Crud
};