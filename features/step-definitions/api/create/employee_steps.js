const { Given, Then, When, setWorldConstructor, setDefaultTimeout, After, Before, And } = require("cucumber");
const { EmployeeWorld } = require("../support/employeeWorld");
const { expect } = require("chai");
const { parseNames, getCamelCaseProperty, generateGuid } = require("../support/helpers");

function getScenarioUniqueString(str, world){
    return `${str}-${world.scenarioUniqueId}`;
}

setWorldConstructor(EmployeeWorld);
setDefaultTimeout(60 * 1000);

Before(async function () {    
    await this.Initialize();
});

Given("I want to create a new employee", () => {
    
});

Given("an active employee", async function () {
    const result = await this.employees.create({
        accountNumber: "",
        address: "",
        bankName: "",
        birthdate: "2020-04-06T20:22:41.052Z",
        city: "Tegucigalpa",
        companyEmail: `test+${this.scenarioUniqueId}@test.com`,
        country: "Honduras",
        displayName: 'Testing',
        effectiveDate: "2020-04-06T20:22:41.052Z",
        firstName: `dummy-${this.scenarioUniqueId}`,
        middleName: `dummy-${this.scenarioUniqueId}`,
        lastName: `dummy-${this.scenarioUniqueId}`,
        secondLastName: `dummy-${this.scenarioUniqueId}`,
        gender: "male",
        personalEmail: "",
        phoneNumber: "",
        region: "Francisco Morazan",
        isActive: true,
        salary: "50000",
        salaryType: "yearly",

        startDate: "2020-04-06T20:22:41.052Z",
        tags: "[]"
    });

    this.setCurrentEmployeeId(result.id);
});

Given('an inactive employee', async function () {
    const result = await this.employees.create({
        accountNumber: "",
        address: "",
        bankName: "",
        birthdate: "2020-04-06T20:22:41.052Z",
        city: "Tegucigalpa",
        companyEmail: `test+${this.scenarioUniqueId}@test.com`,
        country: "Honduras",
        displayName: 'Testing',
        effectiveDate: "2020-04-06T20:22:41.052Z",
        firstName: `dummy-${this.scenarioUniqueId}`,
        middleName: `dummy-${this.scenarioUniqueId}`,
        lastName: `dummy-${this.scenarioUniqueId}`,
        secondLastName: `dummy-${this.scenarioUniqueId}`,
        gender: "male",
        personalEmail: "",
        phoneNumber: "",
        region: "Francisco Morazan",
        isActive: false,
        salary: "50000",
        salaryType: "yearly",

        startDate: "2020-04-06T20:22:41.052Z",
        tags: "[]"
    });

    this.setCurrentEmployeeId(result.id);
});

Then("the employee should exist", async function () {
    var emp = await this.employees.getOne(this.currentEmployeeId);
    expect(emp.id).to.equal(this.currentEmployeeId);
});

Given("the employee's name is {string}", async function (arg1) {    
    var names = makeUniqueToScenario(parseNames(arg1), this);
    await this.employees.changeNames(this.currentEmployeeId, names);
});

async function modifyNames(world, name) {
    var names = parseNames(name);
    var scenarioUniqueNames = makeUniqueToScenario(names, world);
    await world.employees.changeNames(world.currentEmployeeId, scenarioUniqueNames);
}

When("I modify his name to {string}", async function (name) {
    await modifyNames(this, name);
});

When("I attempt to modify his name to {string}", async function (name) {
    try {
        await modifyNames(this, name);
    }
    catch (error) {
        this.setCurrentError(error);
    }
});

When("I modify his address to {string}", async function (newAddressWithCommas) {
    var addressParts = newAddressWithCommas.split(",");
    var changedAddress = {
        address: addressParts[0],
        city: addressParts[1],
        region: addressParts[2],
        country: addressParts[3]
    };
    await this.employees.changeSomething("address", this.currentEmployeeId, changedAddress);
});

Then("the employee's {string} name should be {string}", async function (whichName, expectedName) {
    var foundEmployee = await this.employees.getOne(this.currentEmployeeId);
    if (whichName === "second last") whichName = "secondLast";
    expect(foundEmployee[`${whichName}Name`]).to.equal(getScenarioUniqueString(expectedName, this));
});

Then("the employee's name should be {string}", async function (name) {
    var names = parseNames(name);
    var scenarioUniqueNames = makeUniqueToScenario(names, this);
    var foundEmployee = await this.employees.getOne(this.currentEmployeeId);
    var foundEmployeeNames = {
        firstName: foundEmployee.firstName,
        middleName: foundEmployee.middleName,
        lastName: foundEmployee.lastName,
        secondLastName: foundEmployee.secondLastName
    };
    expect(foundEmployeeNames).to.deep.equal(scenarioUniqueNames);
});

function makeUniqueToScenario(obj, world){
    Object.keys(obj).forEach(key => {
        if(obj[key]){
            obj[key] = getScenarioUniqueString(obj[key], world);
        }
    });
    return obj;
}

async function createEmployeeWithName(world, name) {
    var names = parseNames(name);
    names = makeUniqueToScenario(names, world);

    var employee = await world.employees.create({

        accountNumber: "",
        address: "",
        bankName: "",
        birthdate: "2020-04-06T20:22:41.052Z",
        city: "Tegucigalpa",
        companyEmail: `test+${this.scenarioUniqueId}@test.com`,
        country: "Honduras",
        displayName: names.firstName,
        effectiveDate: "2020-04-06T20:22:41.052Z",
        firstName: names.firstName,
        middleName: names.middleName,
        lastName: names.lastName,
        secondLastName: names.secondLastName,
        gender: "male",
        personalEmail: "",
        phoneNumber: "",
        region: "Francisco Morazan",
        isActive: true,
        salary: "50000",
        salaryType: "yearly",
        
        startDate: "2020-04-06T20:22:41.052Z",
        tags: "[]"
    });

    world.setCurrentEmployeeId(employee.id);
}

When('I create an employee with name {string}', async function (name) {
    await createEmployeeWithName(this, name);
});

When('I attempt to create an employee with name {string}', async function (name) {
    try {
        await createEmployeeWithName(this, name);
    }
    catch (error) {        
        this.setCurrentError(error);
        this.setCurrentEmployeeId(false);
    }
});

Then('it should not be successful', async function () {    
    expect(this.currentError).to.not.be.undefined;
    expect(this.currentError).to.not.be.false;
    expect(this.currentError.status).to.equal(400);
});

Given('there are no employees with name {string}', function (string) {
    //do nothing
});

When('I retrieve employees where {string} is {string}', async function (propertyNameWithSpaces, value) {
    var camelCaseProp = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    payload[camelCaseProp] = value;
    payload["pagesize"] = 10000;
    var emps = await this.employees.getAll(payload);
    this.setCurrentPayloaad(emps);
});

Then('all the employees should have {string} with {string}', async function(propertyNameWithSpaces, value){
    var camelCaseProp = getCamelCaseProperty(propertyNameWithSpaces);
    var retrievedEmployees = this.currentPayload;
    var nonMatches = retrievedEmployees.filter(x=> x[camelCaseProp] !== `${value}-${this.scenarioUniqueId}`);
    expect(nonMatches.length).to.equal(0);
});

Then('the employee\'s {string} should be {string}', async function (propertyNameWithSpaces, value) {
    var expectedValue = value;
    var foundEmployee = await this.employees.getOne(this.currentEmployeeId);
    var prop = getCamelCaseProperty(propertyNameWithSpaces);
    var empValue = foundEmployee[prop];
    expect(empValue).to.equal(expectedValue);
});

async function modifyProp(world, propertyNameWithSpaces, value) {
    var camelCaseProp = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    if(camelCaseProp === "tags"){
        if(!value.includes("[")){
            value = `["${value}"]`;
        }
    }
    
    payload[camelCaseProp] = value;
    await world.employees.changeSomething(camelCaseProp, world.currentEmployeeId, payload);
}

When('I attempt to modify his {string} to {string}', async function (propertyNameWithSpaces, value) {
    try {
        
        await modifyProp(this, propertyNameWithSpaces, value);
    }
    catch (error) {
        this.setCurrentError(error);
        this.setCurrentEmployeeId(false);
    }
});

When('I modify his {string} to {string}', async function (propertyNameWithSpaces, value) {
    await modifyProp(this, propertyNameWithSpaces, value);
});

Given('the employee\'s {string} is {string}', async function (propertyNameWithSpaces, value) {
    var propName = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    if(propName === "tags"){
        if(!value.includes("[")){
            value = `["${value}"]`;
        }
    }
    payload[propName] = value;
    await this.employees.changeSomething(propName, this.currentEmployeeId, payload);
});

When('I retrieve an employee by the Id', async function () {
    var emp = await this.employees.getOne(this.currentEmployeeId);
    this.setCurrentPayloaad(emp);
});

Then('I should get the same employee', function () {
    expect(this.currentPayload.id).to.be.equal(this.currentEmployeeId);
});

When('I deactivate an employee by the Id', async function () {
    await this.employees.deactivate(this.currentEmployeeId, {});
});

When('I activate an employee by the Id', async function () {
    await this.employees.activate(this.currentEmployeeId, {});    
});


Then('I not should be able to see the same employee active', async function () {    
    var emp = await this.employees.getOne(this.currentEmployeeId);
    expect(emp.isActive).to.be.false;
});

Then('I should be able to see the same employee active', async function () {
    var emp = await this.employees.getOne(this.currentEmployeeId);
    expect(emp.isActive).to.be.true;
});
