const axios = require("axios");
const https = require("https");
const sleep = require("sleep");

class EmployeeApi {

    constructor(baseURL, headers) {
        this.sleepAmount = 1;
        this.http = axios.create({
            baseURL,
            timeout: 10000,
            headers,
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });
    }

    async create(employee) {
        var result = await this.request("post", ``, employee);
        sleep.sleep(this.sleepAmount);
        return result;
    }

    async changeNames(id, names) {
        var result = await this.request("put", `${id}/names`, names);
        sleep.sleep(this.sleepAmount);
        return result;
    }

    async changeSomething(prop, id, payload) {
        var result = await this.request("put", `${id}/${prop}`, payload);
        sleep.sleep(this.sleepAmount);
        return result;
    }

    async getAll(conditions){
        var conditionString = Object.keys(conditions).map(key=> `${key}=${conditions[key]}`).join("&");
        var result = await this.request("get", `?${conditionString}`);
        return result;
    }

    async getOne(id){        
        var result = await this.request("get", `${id}`);
        return result;
    }

    async deactivate(id, payload){
        var result = await this.request("put", `${id}/inactive`, payload);
        sleep.sleep(this.sleepAmount);
        return result;
    }

    async activate(id, payload){
        var result = await this.request("put", `${id}/active`, payload);
        sleep.sleep(this.sleepAmount);
        return result;
    }

    async request(verb, route, payload) {
        try {

            var result = await this.http[verb](route, payload);            
            return result.data;
        }
        catch (error) {
            throw {                
                message: `${verb.toUpperCase()} request resulted in an error. ${(typeof error.response !== 'undefined')? error.response.statusText : error.message }.`+ 
                `${(typeof error.response !== 'undefined')? JSON.stringify(error.response.data) : ''}`,
                status: error.response.status,
                method: error.config.method,
                url: error.config.url,
                payload: error.config.data
            };
        }
    }
}
module.exports = {
    EmployeeApi
};