const axios = require("axios");
const https = require("https");
const sleep = require("sleep");
const { generateGuid } = require("./helpers");
const { EmployeeApi } = require("./employeeApi");
const dotenv = require('dotenv');
dotenv.config();

class EmployeeWorld {
    constructor() {        
    }
    
    async Initialize(){        
        const tokenUrl = encodeURI(`https://${process.env.AUTH0_DOMAIN}/oauth/token`);        
        let bearerToken = '';        
        try {
            const result = await axios.post(tokenUrl, 
                {
                    "grant_type":"password",
                    "client_id": process.env.AUTH0_CLIENT_ID,                    
                    "audience": process.env.AUTH0_AUDIENCE_DEV,
                    "username": process.env.AUTH0_CLIENT_USER,
                    "password": process.env.AUTH0_CLIENT_PASS,
                    "scope": "openid"
                });
            bearerToken = result.data.access_token;
        }catch (e) {
            console.error('Error Request:',e);
        }

        this.currentEmployeeId = false;
        this.currentError = false;
        this.currentPayload = false;
        this.payloads = [];
        this.employeeIds = [];
        this.employees = new EmployeeApi(`${process.env.BACKEND_URL}/api/employees`, {
            Authorization: `Bearer ${bearerToken}`
        });        
        this.scenarioUniqueId = generateGuid();
    }

    setCurrentEmployeeId(guid) {
        this.employeeIds.push(guid);
        this.currentEmployeeId = guid;
    }

    setCurrentError(error){
        this.currentError = error;
    }

    setCurrentPayloaad(payload){
        this.payloads.push(payload);
        this.currentPayload = payload;
    }
}

module.exports = {
    EmployeeWorld
};

// setWorldConstructor(CustomWorld);