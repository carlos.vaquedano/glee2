function parseNames(name) {
    var names = name.split(" ");
    var outNames = {
        firstName: "",
        middleName: "",
        lastName: "",
        secondLastName: ""
    };
    switch (names.length) {
        case 1:
            outNames.firstName = names[0];
            break;
        case 2:
            outNames.firstName = names[0];
            outNames.lastName = names[1];
            break;
        case 3:
            outNames.firstName = names[0];
            outNames.middleName = names[1];
            outNames.lastName = names[2];
            break;
        case 4:
            outNames.firstName = names[0];
            outNames.middleName = names[1];
            outNames.lastName = names[2];
            outNames.secondLastName = names[3];
            break;
    }
    return outNames;

}

function generateGuid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function getPascalCaseProperty(propertyNameWithSpaces){
    var words = propertyNameWithSpaces.split(" ");
    var capitolizedWords = words.map(x=> x[0].toUpperCase() + x.substr(1));
    return capitolizedWords.join("");
}

function getCamelCaseProperty(propertyNameWithSpaces){
    var pascalCase = getPascalCaseProperty(propertyNameWithSpaces);
    var camelCase = pascalCase[0].toLowerCase() + pascalCase.substr(1);
    return camelCase;
};

module.exports = {
    parseNames,
    generateGuid,
    getPascalCaseProperty,
    getCamelCaseProperty
};