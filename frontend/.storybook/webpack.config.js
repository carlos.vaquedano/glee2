module.exports = ({ config }) => {    
    config.module.rules.push({
        test: /\.(ts|tsx)$/,
        loader: require.resolve('babel-loader'),
        options: {
            presets: [require.resolve('babel-preset-react-app')],
            plugins: [["@babel/plugin-transform-typescript", {"allowNamespaces": true}]]
        },
    });

config.module.rules.forEach(rule => {
    // See: https://github.com/storybookjs/storybook/issues/6319
    if (rule.test.toString() === '/\\.css$/') {
      const idx = rule.use.findIndex(({ loader }) => loader && loader.includes('css-loader'));
      rule.use[idx].options.modules = true;
    }
  });
    config.node = {
        fs: 'empty'
    };    

    config.resolve.extensions.push('.ts', '.tsx');
    return config;
};