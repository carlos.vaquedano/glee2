import * as React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { Button } from './index';
import style from '../../style.local.css';

export default {
  title: 'Button',
  component: Button,
  decorators: [withKnobs],
};

export const Text = () => (
  <Button onClick={action('clicked')}>Hello World!</Button>
);
export const DisableButton = () => (
  <Button disabled={boolean('Disabled', true)} onClick={action('clicked')}>
    {text('Label', 'Disabled Button')}
  </Button>
);
export const ColorButton = () => (
  <Button
    className={`${style.button} ${style.secondary} ${style.shaddy}`}
    onClick={action('clicked')}
  >
    Styled Button
  </Button>
);
export const ActionsMenuButton = () => (
  <Button>
    <a className={style['btn-icon']}>
      <i className={`${style.icon} ${style['i-more-1']}`} />
    </a>
  </Button>
);

export const IconButton = () => (
  <Button
    className={`${style.button} ${style.primary} ${style.shaddy}`}
    title='Add Employee'
    onClick={action('clicked')}
  >
    <i
      className={`${style.icon} ${style['i-plus']} ${style['margin-right']}`}
      aria-hidden='true'
    />
    Add New Employee
  </Button>
);

export const Emoji = () => (
  <Button onClick={action('clicked')}>
    <span role='img' aria-label='so cool'>
      😀 😎 👍 💯
    </span>
  </Button>
);

Emoji.story = {
  name: 'with emoji',
};
