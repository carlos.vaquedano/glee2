import { WebAuth, Auth0DecodedHash } from 'auth0-js';
import { History } from 'history';
import browserHistory from '../../browserHistory';

export class AuthService {
  auth0: WebAuth;
  storageProvider: Storage;
  private readonly history: History;
  constructor() {
    this.auth0 = new WebAuth({
      clientID: process.env.AUTH0_CLIENT_ID as string,
      domain: process.env.AUTH0_DOMAIN as string,
      redirectUri: process.env.AUTH0_REDIRECT_URI,
      audience: process.env.AUTH0_AUDIENCE,
      responseType: 'token id_token',
      scope: 'openid',
    });
    this.history = browserHistory;
    this.storageProvider = sessionStorage;
  }

  login = () => this.auth0.authorize();

  handleAuthentication = () => {
    const callback: any = (err: any, authResult: Auth0DecodedHash) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        this.history.replace('/employees');
      } else if (err) {
        this.history.replace(`/error?message=${err.errorDescription}`);
        console.error(err);
      }
    };
    this.auth0.parseHash(callback);
  };

  setSession(authResult: Auth0DecodedHash) {
    if (
      !authResult.expiresIn ||
      !authResult.accessToken ||
      !authResult.idToken
    ) {
      return;
    }

    const expiresAt = JSON.stringify(authResult.idTokenPayload.exp * 1000);
    this.storageProvider.setItem('access_token', authResult.accessToken);
    this.storageProvider.setItem('id_token', authResult.idToken);
    this.storageProvider.setItem('expires_at', expiresAt);
  }

  logout = () => {
    this.storageProvider.removeItem('access_token');
    this.storageProvider.removeItem('id_token');
    this.storageProvider.removeItem('expires_at');
    this.auth0.logout({ returnTo: `${process.env.AUTH0_REDIRECT_URI}` });
  };

  isAuthenticated = () => {
    const expires_at = this.storageProvider.getItem('expires_at');
    if (!expires_at) {
      return false;
    }
    const expiresAt = JSON.parse(expires_at);
    return new Date().getTime() < expiresAt;
  };

  getIdToken = () => this.storageProvider.getItem('id_token');

  getAccessToken = () => this.storageProvider.getItem('access_token');
}
