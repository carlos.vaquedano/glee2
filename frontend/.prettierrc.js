module.exports = {
  semi: true,
  arrowParens: 'always',
  trailingComma: 'es5',
  jsxSingleQuote: true,
  singleQuote: true,
  tabWidth: 2,
};
